

words = ('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
    'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z')


class SpellCheck(object):

    _dictionary_words = []
    _control_words = []

    def __init__(self, database=None):

        if database:
            self.__read_file(database)

    @property
    def control_words(self):
        return self._control_words

    @property
    def dictionary_words(self):
        return self._dictionary_words

    def add_value_control_word(self):
        pass

    def add_value_dictionary_word(self):
        pass

    def search(self, word, dictionary):

        if word and dictionary:

            if word not in dictionary:

                return_values = []
                word_size = len(word)

                for x in dictionary:

                    if len(x) >= (word_size - 1) and len(x) <= (word_size + 1):
                        for x in word:

                            value = word.replace(x, '', 1)

                            if (value in dictionary) and \
                                    (not value in return_values):
                                return_values.append(value)

                            for append_word in words:
                                value = word.replace(x, x + append_word, 1)

                                if (value in dictionary) and \
                                        (not value in return_values):
                                    return_values.append(value)

                            for reeplace_word in words:
                                value = word.replace(x, reeplace_word, 1)
                                if (value in dictionary) and \
                                        (not value in return_values):
                                    return_values.append(value)

                return '%s: %s' % (
                    word,
                    ' '.join([str(x) for x in return_values])
                )

            else:
                return '%s is correct' % word

    def __read_file(self, ins):

        first_part = True

        ins = open(ins, 'r')

        for line in ins:
            line = line.replace('\n', '')

            if line == '#':
                first_part = False
                continue

            if first_part:
                self._dictionary_words.append(line)
            else:
                self._control_words.append(line)


speel_check = SpellCheck(
    database='input-spelling-check.txt'
)

control_words = speel_check.control_words  # could be a list
dictionary_words = speel_check.dictionary_words  # could be a list

# dictionary_control = ('at', 'phat', 'haet', 'i',
#'is', 'has', 'have', 'award',
#'pe', 'ha', 'ate', 'ht', 'th', 'hati', 'bat', 'hht')

# control_words = ('m', 'have', 'aware', 'p', 'is')

# you can use speel_check alone!
# speel_check.search('#any word', dictionary_control)

for word in control_words:
    print speel_check.search(word, dictionary_words)
