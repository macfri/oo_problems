class ISBN(object):

    _database = []

    @property
    def database(self):
        return self._database

    def __init__(self, database=None):

        if database:
            self.__read_file(database)

    def check(self, value):

        is_correct = False
        value = value.strip()

        if len(value) > 9 or len(value) < 19:

            has_x = True if value[-1].lower() == 'x' else False
            values = []
            status = False

            for n in value:
                if n.isdigit():
                    values.append(int(n))

            if len(values) == 9:
                if has_x:
                    values.append(10)
                    status = True
                else:
                    status = False

            elif len(values) == 10:
                if has_x:
                    status = False
                else:
                    status = True

            if status:

                sub1 = []
                for x, y in enumerate(values):
                    sub1.append(y + sum(values[:x]))

                sub2 = []
                for x, y in enumerate(sub1):
                    sub2.append(y + sum(sub1[:x]))

                last_value = sub2[-1]

                t = float(last_value) / 11

                if last_value == 0 or t.is_integer():
                    is_correct = True

        return is_correct

    def __read_file(self, ins):

        ins = open(ins, 'r')

        for line in ins:
            line = line.replace('\n', '')
            self._database.append(line.strip())


isbn = ISBN(
    database='input-control-isbn.txt'
)

#print isbn.check('0-13-162959-X')

for value in isbn.database:
    print value, 'es correcto' if isbn.check(value) else 'es incorrecto'
