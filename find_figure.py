class Figure(object):

    _database = []

    @property
    def database(self):
        return self._database

    def __init__(self, database=None):

        if database:
            self.__read_file(database)

    def grid(self, deep):

        _grid = []

        for i in range(1, deep):

            _i = i

            i = _i + sum(range(1, _i - 1))
            j = _i + sum(range(1, _i))

            diff = j - i

            if diff > 0:

                t2 = range(i + 1, j)

                if t2:

                    t2.insert(0, i)
                    t2.insert(len(t2), j)

                    _grid.append(t2)

                else:
                    _grid.append([i, j])
            else:
                _grid.append([i])

        return _grid

    def isolation(self, points):

        g = []
        data = []

        points.sort()

        _minn = min(points)
        _maxx = max(points)

        _grid = self.grid(10)

        for p in _grid:

            short_points = []

            for p2 in points:

                if p2 in p:

                    short_points.append(p2)

            g = g + short_points

            if g:

                if short_points:
                    _min = min(short_points)
                    _max = max(short_points)
                else:
                    _min = 0
                    _max = 0

                data.append([p, (_min, _max)])

                if _maxx in g:
                    break

        return dict(data=data, minn=_minn, maxx=_maxx)

    def find(self, points):

        _points = points
        is_figure = False
        is_par = (float(len(points)) / 2).is_integer()
        size_points = len(points)
        figure = None

        if is_par:
            if size_points == 4:

                figure = 'parallelogram'  # can be parallelogram
            elif size_points == 6:

                figure = 'hexagon'
        else:
            figure = 'triangule'

        # triangule: 3 sides
        # parallelogram: 4 sides
        # hexagon: 6 sides
        # triangule: inpar
        # parallelogram: cuadrado, is_rombo, is_romboide, rectangulo
        # hexagon:

        _isolation = self.isolation(points)

        data = _isolation.get('data')

        i = 0
        n = 0

        tot = 0
        tot_left = 0

        top = True
        is_rombo = False

        ok_middle = False
        ok_button = False

        triangule_one_side = False

        for points, vertices in data:

            if figure == 'parallelogram':

                if i == 0:

                    tot_left = points.index(vertices[0])

                    if vertices[0] == vertices[1]:
                        is_rombo = True
                    else:
                        tot_right = points.index(vertices[1])
                        tot = tot_right - tot_left

                if is_rombo:

                    if top:

                        if (i == 0) or not any(vertices):
                            n += 1
                        else:

                            if points[tot_left] == vertices[0] and \
                                    points[tot_left + n] == vertices[1]:
                                ok_middle = True
                            top = False

                    else:

                        fin = n * 2

                        if i >= fin:
                            if i == fin:

                                if vertices[0] == vertices[1]:
                                    middle_button = tot_left + n

                                    if points[middle_button] == vertices[0]:
                                        ok_button = True
                                        break
                                    else:
                                        ok_button = False
                                        break
                            else:
                                ok_button = False
                                break

                else:

                    if i == tot:
                        middle = points[tot_right]

                        if middle in vertices:

                            left = points[tot_right - tot]
                            right = points[tot_right + tot]

                            if (left in vertices) or (right in vertices):
                                is_figure = True

            elif figure == 'hexagon':

                if i == 0:

                    tot_left = points.index(vertices[0])
                    tot_right = points.index(vertices[1])
                    tot = tot_right - tot_left

                if top:

                    if (i == 0) or not any(vertices):
                        n += 1
                    else:

                        left = points[tot_right - tot]
                        right = points[tot_right + tot]

                        if (left in vertices) and (right in vertices):
                            ok_middle = True

                        top = False

                fin = n * 2

                if i >= fin:
                    if i == fin:

                        if points[tot_right] == vertices[0] and \
                                points[tot_right + tot] == vertices[1]:
                            ok_button = True

            elif figure == 'triangule':

                if i == 0:
                    if vertices[0] == vertices[1]:
                        triangule_one_side = True
                        tot_left = points.index(vertices[0])
                    else:
                        triangule_one_side = False
                        tot_right = points.index(vertices[1])
                        tot_left = points.index(vertices[0])

                        tot = tot_right - tot_left

                if top:
                    if (i == 0) or not any(vertices):
                        n += 1
                    else:
                        top = False

                if not triangule_one_side:

                    if i >= tot:

                        if i == tot:

                            if vertices[0] == vertices[1]:
                                if points[tot_right] == vertices[0]:
                                    ok_button = True
                        else:
                            ok_button = False

                else:

                    if i >= n:
                        if i == n:

                            left = points[tot_left]
                            right = points[tot_left + n]

                            if left == vertices[0] and right == vertices[1]:
                                ok_button = True

                        else:

                            ok_button = False

            i += 1

        if figure == 'parallelogram':
            if is_rombo and ok_middle and ok_button:
                is_figure = True
        elif figure == 'hexagon':
            if ok_middle and ok_button:
                is_figure = True
        elif figure == 'triangule':
            if ok_button:
                is_figure = True

        message = ' '.join([str(x) for x in _points])

        if is_figure:
            message = '%s are the vertices of a %s' % (
                message,
                figure
            )
        else:
            message = '%s are not the vertices of a %s' % (
                message,
                'acceptable figure'
            )

        return message

    def __read_file(self, ins):

        ins = open(ins, 'r')

        for line in ins:
            line = line.replace('\n', '')
            self._database.append(line.strip())


figure = Figure(
    database='input-find-figure.txt'
)

#points = [1, 2, 3]
#print figure.find(points)

for value in figure.database:
    points = [int(x) for x in value.split(' ')]
    print figure.find(points)
